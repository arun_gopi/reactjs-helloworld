# Tutorial Apps
1. [my-app](./my-app/)
   This app helps one to learn the following Reactjs concepts
    
    1. Components
    2. Using JFX and CSS
    3. Working with state
    4. Passing data(props) to components
    5. Pattern to pass data to parent component from child
    6. Populating a list
    7. Showing a list
   This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
   ## How to run the project
    1. `cd my-app`
    2. `npm install`
    3. `npm start`

2. [01-helloworld-rendering](./01-helloworld-rendering/)
3. [02-helloworld-component](./02-helloworld-component/)
4. [03-helloworld-style](./03-helloworld-style/)
5. [04-helloworld-props](./04-helloworld-props/)