'use-strict'

class HelloWorld extends React.Component{

    constructor(props){
        super(props);
        this.state = {username : "Hello World.."};
    }

    render(){
        // note: Rendering without JFX
        return React.createElement('h1', null, this.state.username);
    }
}

const rootContainer = document.querySelector('#root');
const root = ReactDOM.createRoot(rootContainer);
root.render(React.createElement(HelloWorld));