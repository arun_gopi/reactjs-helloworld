/**
   TODO : (1) working with state
   TODO : (2) Using forms
   TODO : (3) Two way binding
   TODO : (4) Sending data to parent component
 */

import {useState} from 'react';
import ListUser from './ListUser';
import './UserDetail.css'
// useState is a React hook and must be called only inside a react componet

const UserDetailForm = (props) =>{

    // Array destructuring
    const [users, setUsers] = useState([]); // Creating state variable (1)

    const [fname, setFname] = useState('');
    const [lname, setLname] = useState('');
    const [country, setCountry] = useState('');

    const clickHandler = () => {
        
        alert(`${users[0].fname}  ${users[0].lname}  ${users[0].country}`);

    };

    const addUserDetailClickHandler = (event) =>{
        event.preventDefault();
        const user = {
            fname : fname,
            lname : lname,
            country : country 
        }
        console.log(user);
        // updating variable in a normal Javascript way wont update the HTML
        // so in React we have to use special 'useState()' function
       setUsers((previousState) => {
          return [...previousState, user];
       }); // this updates state, this causes the component function UserDetailForm to run again and updated vaue is seen in html (1)

       props.onSaveForm(user); // Sending data to parent component (4)

       setFname(''); // clearing inputfield using two way binding (3)
       setLname(''); // (3)
       setCountry(''); // (3)
    };

    const fnameChangeHandler = (event) => {
       setFname(event.target.value);
    };

    const lnameChangeHandler = (event) => {
       setLname(event.target.value);
    };

    const countryChangeHandler = (event) => {
       setCountry(event.target.value);
    };

    return (
        <div className='margin'>

         <form onSubmit={addUserDetailClickHandler}>
            <input onChange={fnameChangeHandler} required type="text" id="fname" name="fname" value={fname}  placeholder='First Name'/>*<br/>
            <input onChange={lnameChangeHandler} required type="text" id="lname" name="lname" value={lname}  placeholder='Last Name'/>*<br/>
            <input onChange={countryChangeHandler} required type="text" id="country" name="country" value={country}  placeholder='Country'/>*<br/>
            <button type='submit' className='margin' >Add User detail</button>   
         </form>   
         
         <button className='margin' onClick={clickHandler}>Get User</button>   

         <ListUser userList={users}></ListUser>
                
        </div>

    );

};

export default UserDetailForm;