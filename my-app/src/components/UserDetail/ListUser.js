/**
 
  TODO : Creating a list (1)
  TODO : Populating list (2)
 */

const ListUser = (props) => {
    return (// (1) (2)
        <div> 
            {props.userList.map((user, index) => <div key={index}> {user.fname} {user.lname} {user.country}</div>)}
        </div>
    );
}

export default ListUser;