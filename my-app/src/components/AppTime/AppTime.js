/**
  TODO : (1) learn passing of properties 
  TODO : (2) calling of functions on events
 */

import './AppTime.css'

var today = new Date();
var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
var dateTime = date + ' ' + time;

var clickMeHandler = ()=>{
    alert('Helloworld');
}

// (1)
function AppTime(props) {
    return (<div>
        <div>Happy Learning!</div><br/>
        <div>
            {props.greeting}
        </div>
        <div className='time'>{dateTime}</div>
        {
          // (2) In react events, functions are used without () 
        }
        <button onClick={clickMeHandler}>Click Me</button>
        </div >);
};

export default AppTime;