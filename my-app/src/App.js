/**
 TODO : (1) Receiveing data from child component
 */

import logo from './logo.svg';
import './App.css';

import './components/AppTime/AppTime'
import AppTime from './components/AppTime/AppTime';
import UserDetailForm from './components/UserDetail/UserDetailForm'

function App() {
  var greeting = "'Have a nice day'";

  // (1) Receiveing data from child component
  const formHandler = (formData) => {

    alert(`${formData.fname}  ${formData.lname}  ${formData.country}`);
  }

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <AppTime greeting={greeting}></AppTime>
        { 
          //(1) Receiveing data from child component
        }
        <UserDetailForm onSaveForm={ formHandler } ></UserDetailForm>
      </header>

      <div>
        <p>Concepts Covered</p>
        <ol>
          <li>Components</li>
          <li>Using JFX and CSS</li>
          <li>Working with state </li>
          <li>Passing data(props) to components</li>
          <li>Pattern to pass data to parent component from child</li>
          <li>Populating a list</li>
          <li>Showing a list</li>
        </ol>
      </div>
      
    </div>
  );
}

export default App;