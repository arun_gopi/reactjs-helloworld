import './AppBar.css'
import PropTypes from 'prop-types';

function AppBar(props){

    return (
      <div className="topbar">
       <div className="app-name">ToDo</div>
       <div className="user-name">
        <span>{props.user.name}</span> {/* TODO: Passings of properties to component */}
        <img className="user-logo" src="https://www.w3schools.com/w3images/avatar2.png" alt="User Logo"/>
       </div>
      </div>
    );

}

AppBar.propTypes= { // TODO: Validation of props object attributes 
  user: PropTypes.shape({
    name: PropTypes.string
  })
}

export default AppBar;